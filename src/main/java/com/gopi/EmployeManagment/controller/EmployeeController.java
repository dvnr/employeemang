package com.gopi.EmployeManagment.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gopi.EmployeManagment.model.Employee;
import com.gopi.EmployeManagment.repository.EmployeeRepository;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@AllArgsConstructor
@RequestMapping("/employee")
@Slf4j
public class EmployeeController {

	private EmployeeRepository employeeRepository;

	@GetMapping("/")
	public List<Employee> getEmployeeList() {

		System.out.println("Get All employess request came");

		return employeeRepository.findAll();
	}

	@GetMapping("/{id}")
	public Optional<Employee> getEmployeeById(@PathVariable(name = "id") long id) {

		log.info("Get employess by id request came : "+id);

		return employeeRepository.findById(id);
	}
	
	@PostMapping("/saveEmployee")
	public Employee saveEmployee(@RequestBody Employee employee) {
		
		log.info("Save employee request came  "+employee);
		
		return employeeRepository.save(employee);
		
	}
	
	
	@PutMapping("/updateEmployee/{id}")
	public ResponseEntity<?> updateEmployee(@RequestBody Employee employee, @PathVariable(name = "id") long id ) {
		
		log.info("Save employee request came  "+employee);
		
		Optional<Employee> emp = employeeRepository.findById(id);
		
		if(emp.isPresent()) {
			Employee dbEmployee = emp.get();
			dbEmployee.setEmail(employee.getEmail());
			
			dbEmployee.setEmpId(id);
			
			dbEmployee.setName(employee.getName());
			return ResponseEntity.ok(employeeRepository.save(employee));
		}
		
		return ResponseEntity.notFound().eTag("No Record found with Id "+id).build();
		
	}
	

}
