package com.gopi.EmployeManagment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class EmployeManagmentApplication extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(applicationClass, args);
		System.out.println("Application started");
	}
	
	@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(applicationClass);
    }

    private static Class<EmployeManagmentApplication> applicationClass = EmployeManagmentApplication.class;

}
