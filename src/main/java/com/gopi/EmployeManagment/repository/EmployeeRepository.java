package com.gopi.EmployeManagment.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gopi.EmployeManagment.model.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long>{

}
